class AddLongitudeToParties < ActiveRecord::Migration[5.1]
  def change
    add_column :parties, :longitude, :decimal
  end
end

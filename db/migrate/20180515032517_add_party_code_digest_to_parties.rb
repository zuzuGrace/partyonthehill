class AddPartyCodeDigestToParties < ActiveRecord::Migration[5.1]
  def change
    add_column :parties, :party_code_digest, :string
  end
end

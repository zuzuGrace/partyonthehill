class CreateHosts < ActiveRecord::Migration[5.1]
  def change
    create_table :hosts do |t|
      t.string :username
      t.string :email
      t.boolean :is_admin

      t.timestamps
    end
  end
end

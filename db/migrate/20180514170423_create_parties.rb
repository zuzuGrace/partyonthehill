class CreateParties < ActiveRecord::Migration[5.1]
  def change
    create_table :parties do |t|
      t.string :tag
      t.string :address
      t.datetime :date
      t.string :start_time
      t.string :end_time
      t.boolean :is_public
      t.decimal :rating
      t.references :host, foreign_key: true

      t.timestamps
    end
  end
end


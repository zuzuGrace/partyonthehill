class AddLatitudeToParties < ActiveRecord::Migration[5.1]
  def change
    add_column :parties, :latitude, :decimal
  end
end

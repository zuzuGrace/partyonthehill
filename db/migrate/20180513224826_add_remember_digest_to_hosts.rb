class AddRememberDigestToHosts < ActiveRecord::Migration[5.1]
  def change
    add_column :hosts, :remember_digest, :string
  end
end

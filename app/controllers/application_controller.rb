class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include GuestSessionsHelper
  include SessionsHelper
end

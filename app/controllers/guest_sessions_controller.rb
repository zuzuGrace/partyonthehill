class GuestSessionsController < ApplicationController
	def create
		Stripe.api_key = "sk_test_4WpGBozjMj2Fp8hJSNR2E63N"
		token = params[:stripeToken]

		@charge = Stripe::Charge.create({
    			amount: 99,
    			currency: 'usd',
    			description: 'Example charge',
    			source: token,
		})

		if !@charge.nil?
			create_guest_session
			redirect_to party_rock_path
		else 
			flash.now[:danger] = "Your card could not issue a charge. Please try again."
		end
	end

	def destroy
	end
end

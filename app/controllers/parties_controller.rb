class PartiesController < ApplicationController
	before_action :end_guest_session, only: [:index]
	#display all parties
	def index
		@parties = Party.all
	end

	def new
	end

	def create
		Stripe.api_key = "sk_test_4WpGBozjMj2Fp8hJSNR2E63N"
		token = params[:stripeToken]

		@charge = Stripe::Charge.create({
    			amount: 299,
    			currency: 'usd',
    			description: 'Host ',
    			source: token,
		})

		if !@charge.nil?
			@party = Party.new(party_params)
			@party.rating = 0
			@party.create_code #calls the party's create function to 
			if @party.save
			#generated party code page
			redirect_to @party
			else
				flash[:error] = "Unable to create your event. Please try again."
			end
		end
	end

	def show
		@party = Party.find(params[:id])
	end

	def edit
		@party = Party.find(params[:id])
	end

	def update
		@party = Party.find(params[:id])
		if @party.update_attributes(party_params)
			flash[:success] = "Profile Updated"
			redirect_to @party
		else 
			flash[:error] = "Unable to update your account"
		end
	end

	def party_code
		party = Party.find_by(party_code: params[:party_code])
		if !party.nil? #party exists
			redirect_to party
		else
			flash[:error] = "Unable to find your event. It might not exist or you entered the wrong code."
		end
	end

	def destroy
		@party = Party.find(params[:id])
		@party.destroy
		#if logged in redirect_to hosts page if not redirect to 
	end

	private
		#get party parameters when a party is created
		def party_params
			params.require(:party).permit(:tag, :address, :date, :start_time,
                                   :end_time, :is_public, :rating)
		end


		#get the party code to allow access to the party page
		def party_code_params
			params.require(:party).permit(:party_code)
		end

		def end_guest_session
			if  end_access < Time.now
				redirect_to root_url
			end
		end
end

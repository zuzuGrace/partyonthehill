class HostsController < ApplicationController
	before_action :logged_in_user, only: [:edit, :update, :destroy]
	before_action :correct_user,   only: [:edit, :update]
	before_action :admin_user,     only: :destroy

	def new
		@host = Host.new
	end

	def create
		@host = Host.new(host_params)
		@host.is_admin = true
		if @host.save
			flash[:success] = "We like you...ALOT"
			log_in @host
			redirect_to @host
		else
			redirect_to home_path
			flash[:error] = "Unable to create your account. Please try again."
		end
	end

	def show
		@host = Host.find(params[:id])
		@myEvents = Party.find_by(host_id: @host.id)
	end

	def edit
		@host = Host.find(params[:id])
	end

	def update
		@host = Host.find(params[:id])
		if @host.update_attributes(host_params)
			flash[:success] = "Profile Updated"
			redirect_to @host
		else 
			flash[:error] = "Unable to update your account"
		end
	end

	def destroy
		@host = Host.find(params[:id])
		@host.destroy
		redirect_to root_url
	end

	private
		def host_params
			params.require(:host).permit(:username, :email, :password, :password_confirmation)
		end

		def logged_in_user
      		unless logged_in?
       		 	flash[:danger] = "Please log in."
        		redirect_to login_url
      		end
    	end

    	def correct_user
     		 @host = Host.find(params[:id])
     		 redirect_to(root_url) unless current_host?(@host)
   		end

   		def admin_user
      		redirect_to(root_url) unless current_host.admin?
    	end
end

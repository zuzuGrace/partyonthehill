class SessionsController < ApplicationController
  def new
  end

  def create
  	host = Host.find_by(email: params[:session][:email].downcase)
    if host && host.authenticate(params[:session][:password])
      # Log the user in and redirect to the user's show page.
          log_in host
          params[:session][:remember_me] == '1' ? remember(host) : forget(host)
          redirect_to host
    else
      # Create an error message.
          flash.now[:danger] = 'Invalid email/password combination'
          redirect_to home_path
        end
  end

  def destroy
  	log_out if logged_in?
  	redirect_to root_url
  end
end

module SessionsHelper

	def log_in(host)
    	session[:host_id] = host.id
  	end

  	def current_host
    	if (host_id = session[:host_id])
      		@current_host ||= Host.find_by(id: host_id)
    	elsif (host_id = cookies.signed[:host_id])
     		 host = Host.find_by(id: host_id)
      		if host && host.authenticated?(cookies[:remember_token])
        		log_in host
       			 @current_host = host
      		end
    	end
  	end

  	def remember(host)
    	host.remember
    	cookies.permanent.signed[:host_id] = host.id
    	cookies.permanent[:remember_token] = host.remember_token
  	end

  	def forget(host)
    	host.forget
    	cookies.delete(:host_id)
    	cookies.delete(:remember_token)
  	end

  	def current_host?(host)
    	host == current_host
 	 end


  	def logged_in?
  		!current_host.nil?
  	end

  	def log_out
  		forget(current_host)
  		session.delete(:host_id)
  		@current_host = nil
  	end
end

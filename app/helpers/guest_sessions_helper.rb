module GuestSessionsHelper
	# creates a 45 minute timed session for the given user
  def create_guest_session
    session[:expires_at] = Time.now + 2 * 60
  end

  #ends the user session after specified time
  def end_access
  	time = session[:expires_at]
  end

  def time_split
  	sp = end_access.split("T")
  	sp[1].split(".")[0]
  end


end

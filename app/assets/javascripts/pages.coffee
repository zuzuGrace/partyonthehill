# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on "turbolinks:load", ->
			 $('#invite-content').fadeIn(200)

			 $('.opt-tags').click ->
			 	$('.options').hide()
			 	$('.host-dash').hide()
			 	$('.close-bttn').hide()
			 	if @id == 'rate'
			 		$('#rate-opts').fadeIn(200)
			 		$('.close-bttn').fadeIn(200)
			 	else if @id == 'guest'
			 		$('#guest-opts').fadeIn(200)
			 		$('.close-bttn').fadeIn(200)
			 	else if @id == 'acct'
			 		$('#acct-host').fadeIn(200)
			 		$('.close-bttn').fadeIn(200)
			 	else if @id == 'onetime'
			 		$('#host-opts').fadeIn(200)
			 		$('.close-bttn').fadeIn(200)
			 	else if @id == 'logInCode'
			 		$('#logIn').fadeIn(200)
			 		$('.close-bttn').fadeIn(200)

			 $('.opt-tags').click ->
			 	$('.host-viewer').removeClass('slideOutViewer')
			 	$('.host-dash').hide()
			 	if @id == 'host-event'
			 		$('.host-viewer').addClass('slideOutViewer')
			 		$('#host-event-opts').fadeIn(200)
			 	else if @id == 'host-myEvents'
			 		$('.host-viewer').addClass('slideOutViewer')
			 		$('#host-myEvent-opts').fadeIn(200)
			 	else if @id == 'host-acct-settings'
			 		$('.host-viewer').addClass('slideOutViewer')
			 		$('#host-acct-opts').fadeIn(200)
	

			 $('.close-bttn').click ->
			 	$('.options').hide()
			 	$(this).hide()

			 $('.opt-tags').mouseenter ->
			 	$(this).css("letter-spacing", "3px")
			 	$(this).css("color", "#f441dc")

			 $('.opt-tags').mouseleave ->
			 	$(this).css("letter-spacing", "1px")
			 	$(this).css("color", "#013349")
			 
			 $('.arrows').click ->
			 	$('#form-login').slideUp(200)
			 	$('#party_code').slideUp(3000)

			 $('#party-code').mouseenter ->
			 	$(this).css("letter-spacing", "3px")

			 $('#party-code').mouseleave ->
			 	$(this).css("letter-spacing", "1px")

			 $('#one-time-host-payment-header').mouseenter ->
			 	$(this).css("letter-spacing", "3px")

			 $('#one-time-host-payment-header').mouseleave ->
			 	$(this).css("letter-spacing", "1px")

			 $('#one-time-host-back').mouseenter ->
			 	$(this).css("letter-spacing", "3px")

			 $('#one-time-host-back').mouseleave ->
 				$(this).css("letter-spacing", "1px")
 		     	


			 $('#logIn-slide').mouseenter ->	
			 	$(this).css("letter-spacing", "3px")
			 $('#logIn-slide').mouseleave ->
			 	$(this).css("letter-spacing", "1px")

			 $('#party-code').click ->
			 	$('#form-login').slideUp(1000)
			 	$('#party_code').slideUp(3000)
			 	$('#party-code-edit').fadeIn(300)

			 $('#one-time-host-payment-header').click ->
			 	$('#one-time-host-form').slideUp(1000)
			 	$('#one-time-host-payment-header').slideUp(3000)
			 	$('#one-time-host-payment').fadeIn(300)

			 $('#logIn-slide').click ->
			 	$('#form-login').slideDown(1000)
			 	$('#party_code').slideDown(1000)
			 	$('#party-code-edit').fadeOut(200)

			 $('#one-time-host-back').click ->
 		  	 	$('#one-time-host-form').slideDown(1000)
 		  	 	$('#one-time-host-payment-header').slideDown(1000)
 		  	 	$('#one-time-host-payment').fadeOut(200)


    
		
 

			 document.getElementById('invite').innerHTML = ''
			 i = 0
			 txt = 'There\'s a Party on the Hill, will you come?'
			 speed = 50
			 typeWriter = ->
			 	if i < txt.length
			 		document.getElementById('invite').innerHTML += txt.charAt(i)
			 		i++
			 		setTimeout typeWriter, speed
			 	return

			 do typeWriter

			 $('#invite-content').delay(1000).css("top", "200px")



class Party < ApplicationRecord
attr_accessor :party_code_token
  belongs_to :host
  validates :tag,  presence: true
  validates :address,  presence: true
  validates :date,  presence: true
  validates :start_time,  presence: true
  validates :end_time,  presence: true
  validates :is_public,  presence: true
  validates :rating,  presence: true
  
  #returns a random token for the party_code
  def Party.new_token
  	SecureRandom.urlsafe_base64
  end

  def create_code
  	self.party_code_token = Party.new_token
    update_attribute(:party_code_digest, Party.digest(party_code_token))
  end

  def authenticated?(party_code_token)
  	return false if party_code_digest.nil?
    BCrypt::Password.new(party_code_digest).is_password?(party_code_token)
  end


end

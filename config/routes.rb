Rails.application.routes.draw do
  root 'pages#invite'

  get '/home', to: 'pages#home'

  get '/about', to: 'pages#about'

  post '/guest_access', to: 'guest_sessions#create'

  delete '/guest_bye', to: 'guest_sessions#destroy'

  get '/party_rock', to: 'parties#index'

  post '/party_upon', to: 'parties#create'

  post '/party_code', to: 'parties#party_code'

  post '/a_thing', to: 'hosts#create'

  get '/login', to: 'sessions#new'

  post '/login', to: 'sessions#create'

  delete '/logout', to: 'sessions#destroy'

  resources :hosts do
  	resources :parties
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
